import React, {Component} from 'react'
import {FaPen, FaTrashAlt, FaSave} from 'react-icons/fa'
import {useState} from 'react'

// !!! we store state data in the parent component, we pass it to child components through props and we send data
// from child to parent through events (this is how it works without an application store)

class Note extends Component{
    constructor(props){
        super(props)

        this.state={
            editing: false 
        }

        this.edit = this.edit.bind(this)
        this.remove = this.remove.bind(this)
        this.renderForm = this.renderForm.bind(this)
        this.renderDisplay = this.renderDisplay.bind(this)
        this.save = this.save.bind(this)    
        this.randomBetween = this.randomBetween.bind(this)
    }

    componentWillMount(){
        this.style = {
            right: this.randomBetween(0, window.innerWidth - 150, 'px'),
            top: this.randomBetween(0, window.innerHeight - 150, 'px'),
            transform: `rotate(${this.randomBetween(-25, 25, 'deg')})`
        }
    }
    
    // method to display the notes randomly on the screen
    randomBetween(x, y, s) {
        return x + Math.ceil(Math.random() * (y-x)) + s
    }

    componentDidUpdate(){
        var textArea
        if(this.state.editing){
            textArea = this._newText
            textArea.focus()
            textArea.select()
        }
    }

    // if we're not using this, if we update a note and live the text as it is, the component will still re-render
    // this will check to see if something has changed and if it did, it will re-render, if not, it won't
    shouldComponentUpdate(nextProps, nextState){
        return(
            this.props.children !== nextProps.children || this.state !== nextState
        )

    }

    edit(){
        this.setState({
            editing: true
        })
    }

    remove(){
        this.props.onRemove(this.props.index)
    }

    save(e){
        //this will prevent the default behaviour of the form 
        e.preventDefault()

        // this data is passed as props from the onChange action of notes defined in the board component
        // remember this is why we have ref in the textarea to take the new value of the note text
        // the updating se face in Board component and we pass the new text and the note's id to it
        this.props.onChange(this._newText.value, this.props.index)

        // we also have to set back the state editing property to false
        this.setState({
            editing: false
        })
    }

    renderForm(){
        return(
            <div className="note" style={this.style}>
                <form onSubmit={this.save}>
                    <textarea ref={input => this._newText = input}
                              defaultValue={this.props.children}/>
                    <button id="save"><FaSave /></button>
                </form>
            </div>
        )
    }

     renderDisplay() {
         console.log(this.props.children)
         return(
             <div className="note" style={this.style}>
                {/*this means it will display anything that is a child element of that  */}
                {/*adica in Board display-uim note.note deci cumva note.note ii info about this note  */}
                <p>{this.props.children}</p>
                 <span>
                    <button onClick={this.edit} id="edit"><FaPen /></button>
                   <button onClick={this.remove} id="remove"><FaTrashAlt /></button>
                </span>
             </div>
         )
    }

    render() {
        // this is exactly the method below only much cleaner
        return this.state.editing ? this.renderForm() : this.renderDisplay()

        // if(this.state.editing){
        //     return this.renderForm()
        // } else {
        //     return this.renderDisplay()
        // }
         
    }
  
     
}

export default Note
