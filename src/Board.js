import React, {Component} from 'react'
import Note from './Note'
import {FaPlusSquare} from 'react-icons/fa'


class Board extends Component{
    constructor(props){
        super(props)
        this.state = {
            notes: []
        }

        this.eachNote = this.eachNote.bind(this)
        this.update = this.update.bind(this)
        this.remove = this.remove.bind(this)
        this.add = this.add.bind(this)
        this.nextId = this.nextId.bind(this)
    }

    componentWillMount(){
        var self = this
        if(this.props.count){
            fetch(`https://baconipsum.com/api/?type=all-meat&sentences=${this.props.count}`)
                .then(response => response.json())
                .then(json => json[0]
                    .split('. ')
                    .forEach(sentence => self.add(sentence.substring(0,25))))
        }
    }

    add(text){
        this.setState(prevState => ({
            // we create a new array adding to it the previously existing notes (using the es6 spread operator ...)
            // & add a new object to it (a note) with an id and the text
            notes:[
                ...prevState.notes,
                {
                    id: this.nextId(),
                    note: text
                }
            ]
        }))
    }

    nextId() {
        this.uniqueId = this.uniqueId || 0
        return this.uniqueId++
    }

    // this method will have to send the info back to the note and update it
    update(newText, i){
        console.log('updating item at index', i, newText)

        //inside of the setState function we'll use a callback function
        // the callback function will take the previous state (which is the current array of notes) 
        this.setState(prevState => ({
            // we reset the notes to their previous state and only change the edited one
            notes: prevState.notes.map(
                // for each item in the array we'll pass in the note

                // if the note's id is not equal to the i, it means it's not the one we want to edit and just return it
                // if it is the one we're looking for we return a new object which passes in all the keys of the note 
                // but will overwrite the text of the note key of the note

                note => (note.id !== i) ? note : {...note, note: newText}
            )
        }))


    }

    remove(id){
        console.log("Removing item at", id)

        this.setState(prevState => ({
            // this will return a new array with the elements which do not match the condition (aka the ones we don't want to remove)
            notes: prevState.notes.filter(note => note.id !== id)
        }))
    }

    eachNote(note, i){
        return(

            // this is how we get the new text from the note (with onChange) & know which note to update
            // the onChange passes the data as props to the Note component where it will update (save) the info
            <Note key={note.id}
                  index={note.id}
                  onChange={this.update}
                  onRemove={this.remove}>{note.note}</Note>
        )
    }

    render(){
        return (
            <div className="board">
                {this.state.notes.map(this.eachNote)}

                {/* we bind it so that each time a new note is created, it has default text */}
                <button onClick={this.add.bind(null, 'New Note')}
                        id="add">
                        <FaPlusSquare /></button>
            </div>
        )
    }
}

export default Board